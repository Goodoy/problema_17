#include <iostream>

/*Se recibe un numero y se realiza un rastreo desde 1 buscando los numeros amigos, esto se
  hace buscando sus divisores, luego sumandolos y a este numero encontrado, buscarle sus
  divisores y por ultimo ver si este numero resultante es igual a original, en caso de serlo
  es amigo y se almacena (Verificando que no halla estado antes) en caso de no serlo se
  continua con el siguiente.*/

using namespace std;


int* calcularDivisores(long int n); //Recibe un int y devueve un puntero al arreglo de divisores
long int sumaDivisores(int *div);  //Recibe un puntero con divisores y devuelve la suma de ellos
void imprimirAmigos(int mat[][2]); //Funcion opcional para ver de forma matricial los amigos
bool compExistencia(long int n,int mat[][2]); //Verfica si ya se habia encontrado estos numeros
long int sumarAmigos(int mat[][2]); //Recibe un arreglo y suma todos sus componentes

int main()
{

    int *ptrDivA; //Almacenado de divisores (Caso extremo 1000)
    int *ptrDivB;  //Almacenado de divisores (Caso extremo 1000)
    long int sumA,sumB;  //Suma de los divisores de A y B.

    int matAmigos[100][2]={0}; //Arreglo que guarda los numeros amigos para no repetir
    int filas=0; //Fila de numeros amigos
    long int numero=0; //numero limite
    cout <<"Ingrese un numero :"<<endl;
    cin >> numero; //Recibe el numero


    for(long int i=1;i<numero ;i++){ //Recorre todo los numeros desde 1 hasta numero
        ptrDivA=calcularDivisores(i);  //Se esta referenciando el arreglo creado con los divisores de i
        sumA=sumaDivisores(ptrDivA);  //Suma de los divisores de i
        ptrDivB=calcularDivisores(sumA); //Se esta referenciando el arreglo creado con los divisores SumA
        sumB=sumaDivisores(ptrDivB); //Suma de los divisores de sumA ---> sumB
        if(sumB==i&&(i!=sumA)){ //Si i == sumB, pero diferente de si mismo Ejm 6
            if(!compExistencia(sumA,matAmigos)){ //
                matAmigos[filas][0]=i;
                matAmigos[filas][1]=sumA;
                filas++;
            }

        }
    }

    //imprimirAmigos(matAmigos); opcional para verificar

    cout<<"El resultado de la suma es "<<sumarAmigos(matAmigos)<<endl;

    return 0;
}

int* calcularDivisores(long int n){
    int *divisores = new int[100]; //Crear un puntero a una ubicacion de tamaño dinamico 100
    int pos=0;
    for(int i=1;i<n;i++){
        if(n%i==0){ //Verifica si es divisor
            *(divisores+pos)=i; //Guarda el i en la posicion pos
            pos++;
        }
    }
    return divisores;
}

long int sumaDivisores(int *div){
    int pos=0;
    int sum=0;
    while(*(div+pos)!=0){ //Recorre el arreglo hasta 0 (Que es el ultimo numero del arreglo)
        sum+=*(div+pos); //Mientras los recorre los va sumando en un acumulador
        pos++; //Pasa al siguiente indice
    }
    return sum;
}

void imprimirAmigos(int mat[][2]){ //Impresion opcional.
    int k=0;

    cout<<"Numero Amigos\n a\t|\tb\n";
    while(mat[k][0]!=0 && mat[k][0]!=0){
        cout <<mat[k][0]<<"\t|\t"<<mat[k][1]<<endl;
        k++;
    }
}

long int sumarAmigos(int mat[][2]){
    int pos=0;
    long int sum=0;
    while(mat[pos][0]!=0 && mat[pos][0]!=0){ //Verifica que termino de recorrer.
        sum+= mat[pos][0]+mat[pos][1]; //Va sumando cada una de las posiciones
        pos++; //Pasa a la siguiente posicion
    }
    return sum;
}

bool compExistencia(long int n,int mat[][2]){
    int pos=0;
    while(mat[pos][0]!=0 && mat[pos][0]!=0){ //Recorre hasta encontrar 0
        if(mat[pos][0]==n){ //Verifica si antes estaba
            return true;
        }else{
            pos++; //Siguiente posicion
        }
    }
    return false;
}
